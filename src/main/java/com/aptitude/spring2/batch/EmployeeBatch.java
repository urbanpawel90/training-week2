package com.aptitude.spring2.batch;

import com.aptitude.spring2.employee.Employee;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.sql.DataSource;

@Configuration
public class EmployeeBatch {
    @Bean
    public ItemReader<Employee> employeeItemReader(DataSource dataSource) {
        JdbcCursorItemReader<Employee> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql("SELECT * FROM employee");
        reader.setRowMapper(new BeanPropertyRowMapper<>(Employee.class));
        return reader;
    }

    @Bean
    public ItemProcessor<Employee, Employee> employeeProcessor() {
        return item -> {
            item.setSalary(item.getSalary() * 2);
            return item;
        };
    }

    @Bean
    @Scope("prototype")
    public ItemWriter<Employee> employeeCsvWriter() {
        FlatFileItemWriter<Employee> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource("employees" + System.currentTimeMillis() + ".csv"));

        DelimitedLineAggregator<Employee> aggregator = new DelimitedLineAggregator<>();
        aggregator.setDelimiter(",");
        BeanWrapperFieldExtractor<Employee> fields = new BeanWrapperFieldExtractor<>();
        fields.setNames(new String[]{"name", "surname", "salary", "city"});
        aggregator.setFieldExtractor(fields);
        writer.setLineAggregator(aggregator);
        return writer;
    }

    @Bean
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
    @Qualifier("job1")
    public Job jdbcToCsvJob(JobBuilderFactory jobBuilder,
                            StepBuilderFactory stepBuilder,
                            DataSource dataSource,
                            JobRepository jobRepository) {
        Step jobStep = stepBuilder.get("step1")
                .<Employee, Employee>chunk(5)
                .reader(employeeItemReader(dataSource))
                .processor(employeeProcessor())
                .writer(employeeCsvWriter())
                .build();
        return jobBuilder.get("jdbcToCsv")
                .incrementer(new RunIdIncrementer())
                .repository(jobRepository)
                .start(jobStep)
                .build();
    }
}
