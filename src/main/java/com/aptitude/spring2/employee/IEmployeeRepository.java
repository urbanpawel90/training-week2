package com.aptitude.spring2.employee;

import java.util.List;

public interface IEmployeeRepository {
    void save(List<Employee> employees);
    List<Employee> getAll();
}
