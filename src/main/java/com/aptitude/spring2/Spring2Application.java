package com.aptitude.spring2;

import com.aptitude.spring2.employee.IEmployeeRepository;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableBatchProcessing
@EnableScheduling
public class Spring2Application {
    @Autowired
    private IEmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(Spring2Application.class, args);
    }
}
