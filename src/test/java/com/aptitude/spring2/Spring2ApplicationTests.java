package com.aptitude.spring2;

import com.aptitude.spring2.employee.Employee;
import com.aptitude.spring2.employee.EmployeesRestController;
import com.aptitude.spring2.employee.IEmployeeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

//@RunWith(MockitoJUnitRunner.class)
public class Spring2ApplicationTests {
    @Mock
    private IEmployeeRepository mockedRepository;
    @InjectMocks
    private EmployeesRestController controller;
    @Captor
    private ArgumentCaptor<List<Employee>> employeeListCapture;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(mockedRepository.getAll()).thenReturn(Collections.emptyList());
    }

    @Test
    public void test_verify_listEmployees_repositoryInteractions() {
        controller.listEmployees();

        verify(mockedRepository, times(1)).getAll();
        verifyNoMoreInteractions(mockedRepository);
    }

    @Test
    public void test_verify_createEmployee() {
        Employee testEmployee = new Employee("Jan", "Kowalski", "Miasto", 1000);

        controller.addEmployee(testEmployee.getName(), testEmployee.getSurname(),
                testEmployee.getCity(), testEmployee.getSalary());

        verify(mockedRepository).save(employeeListCapture.capture());

        List<Employee> captured = employeeListCapture.getValue();
        assertThat(captured, hasSize(1));
        assertThat(captured.get(0).getName(), is(testEmployee.getName()));
    }

}
